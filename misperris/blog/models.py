from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext as _

# Create your models here.

class Postulante(models.Model):
    rut= models.CharField(max_length=20)
    nombre = models.CharField(max_length=20)
    correo = models.CharField(max_length=20)
    numero = models.IntegerField()
    fecha_Nacimiento = models.DateTimeField(
            default=timezone.now)
    Region = models.CharField(max_length=20)
    Comuna = models.CharField(max_length=20)
    tipoVivienda =models.CharField(max_length=20)
    class Meta:
        permissions = (
            ('postulante', _('Postulante')),
        )  

#falta fotografia
class Perris(models.Model):
        foto = models.FileField()
        nombre = models.CharField(max_length=20)
        raza = models.CharField(max_length=20)
        descripcion = models.TextField(max_length=200)
        estado = models.CharField(max_length=20)
