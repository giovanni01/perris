from django.shortcuts import render,redirect,get_object_or_404
from django.utils import timezone
from .models import Postulante
from .models import Perris
from .forms import PostulanteForm,PerrosForm
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
# Create your views here.
def post_list(request):
    user = request.user
    posts = Postulante.objects
    if user.has_perm('blog.Postulante'):
        return render(request, 'blog/index.html', {'posts': posts}) 
    else:
        return render(request, 'blog/index.html')  


def Postulante_view(request):
    if request.method == 'POST':
        form = PostulanteForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.save()
            login(request, user)
        return redirect('http://127.0.0.1:8000')
    else:
        form = PostulanteForm()
    return render(request,'blog/formperris.html',{'form':form}) 


def login_view(request):
    if request.method =='POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user() 
            login(request, user)
            return redirect('http://127.0.0.1:8000')
    else:
        form = AuthenticationForm()
    return render(request,'blog/login.html',{'form':form})   

def listar_perros(request):
    posts=Perris.objects.filter()
    return render(request, 'blog/post_list.html',{'posts':posts})

def listar_rescatados(request):
    posts=Perris.objects.filter()
    return render(request, 'blog/rescatados.html',{'posts':posts})

def listar_adoptados(request):
    posts=Perris.objects.filter()
    return render(request, 'blog/adoptado.html',{'posts':posts})

def listar_disponibles(request):
    posts=Perris.objects.filter()
    return render(request, 'blog/disponibles.html',{'posts':posts})

def perris_view(request):
    user = request.user
    posts = Postulante.objects
    if user.has_perm('blog.Postulante'):
        if request.method =='POST':
            user = request.user
            form = PerrosForm(request.POST, request.FILES)
            if form.is_valid():
                post = Perris(foto = request.FILES['foto'],nombre=request.POST['nombre'] ,raza=request.POST['raza'],descripcion=request.POST['descripcion'],estado=request.POST['estado'])
                post.save(form)
            return redirect('http://127.0.0.1:8000/listarp')
        else:
            form = PerrosForm()
        return render(request,'blog/formperro.html',{'form':form})   
    else:
        return render(request, 'blog/postulante.html')
def perris_edit (request, id_mascota):
    perro= Perris.objects.get(id=id_mascota)
    user = request.user
    posts = Postulante.objects
    if user.has_perm('blog.Postulante'):
        if request.method == "POST":
            user = request.user
            form = PerrosForm(request.POST, instance=perro )
            if form.is_valid():
            
                form.save()
                return redirect ('http://127.0.0.1:8000/listarp')
        else:
            form = PerrosForm(instance=perro)
        return render (request, 'blog/formperro.html', {'form':form})   
    else:
        return render(request, 'blog/postulante.html')   
def delete_perris (request,id_mascota):
    perro= Perris.objects.get(id=id_mascota)
    user = request.user
    posts = Postulante.objects
    if user.has_perm('blog.Postulante'):
        if request.method == 'POST':    
            user = request.user 
            perro.delete()
            return redirect ('http://127.0.0.1:8000/listarp')
        else:
            return render (request, 'blog/perris_delete.html', {'perro':perro})    
    else:
        return render(request, 'blog/postulante.html')       

    def base_layout(request):
	       template='blog/disponibles.html'
	       return render(request,template)   