from django import forms
from .models import Postulante, Perris

class PostulanteForm(forms.ModelForm):
    class Meta:
        model = Postulante

        fields = [
            'rut',
            'nombre', 
            'correo', 
            'numero', 
            'fecha_Nacimiento', 
            'Region', 
            'Comuna',
            'tipoVivienda',
        ]
        labels ={
            'rut': 'RUT',
            'nombre':'Nombre', 
            'correo':'Correo', 
            'numero': 'Numero', 
            'fecha_Nacimiento': 'Fecha Nacimiento', 
            'Region': 'Region', 
            'Comuna': 'Comuna',
            'tipoVivienda': 'Tipo de Vivienda',
        }
        widgets = {
            'rut': forms.TextInput(attrs={'class':'form-control'}),
            'nombre':forms.TextInput(attrs={'class':'form-control'}), 
            'correo':forms.TextInput(attrs={'class':'form-control'}), 
            'numero':forms.NumberInput(attrs={'class':'form-control'}),
            'fecha_Nacimiento':forms.DateInput(attrs={'class':'form-control'}), 
            'Region':forms.Select(attrs={'id':'regiones'}), 
            'Comuna':forms.Select(attrs={'id':'comunas'}), 
            'tipoVivienda':forms.TextInput(attrs={'class':'form-control'}),
        }

class PerrosForm(forms.ModelForm):
    class Meta:
        model = Perris

        fields = [
        'foto',
        'nombre', 
        'raza',
        'descripcion',
        'estado', 
        ]
        labels ={
        'foto': 'FOTO',
        'nombre': 'Nombre', 
        'raza': 'Raza',
        'descripcion': 'Descripcion',
        'estado': 'Estado' ,  
        }
        widgets = {
        
        'nombre':forms.TextInput(attrs={'class':'form-control'}), 
        'raza':forms.TextInput(attrs={'class':'form-control'}),
        'descripcion':forms.TextInput(attrs={'class':'form-control'}),
        'estado':forms.TextInput(attrs={'class':'form-control'}),  
        }
