#from django.contrib.auth.models import User
from blog.models import Postulante
from rest_framework import serializers

class PostulanteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Postulante
        fields = ('url','rut', 'nombre','correo','fecha_Nacimiento','Region','Comuna','tipoVivienda')


