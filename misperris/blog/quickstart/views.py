#from django.contrib.auth.models import User
from blog.models import Postulante
from rest_framework import viewsets
from blog.quickstart.serializers import PostulanteSerializer

class PostulanteViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Postulante.objects.all()
    serializer_class = PostulanteSerializer


