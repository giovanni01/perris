from django.conf.urls import include, url
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path,include

urlpatterns = [
    path('', include('pwa.urls')),
    url(r'^$', views.post_list,name='perris'),
    url(r'^listarp$', views.listar_perros),
    url(r'^listar_res$', views.listar_rescatados),
    url(r'^listar_adop$', views.listar_adoptados),
    url(r'^listar_disp$', views.listar_disponibles),
    url(r'^nuevo$', views.Postulante_view),
    url(r'^nuevo_perro$', views.perris_view),
    url(r'^login/$', views.login_view, name="login"),
    url(r'^editar/(?P<id_mascota>\d+)/$', views.perris_edit, name='post_edit'),
    url(r'^eliminar/(?P<id_mascota>\d+)/$', views.delete_perris, name='post_delete'),
    url('accounts/', include('allauth.urls')),  
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)