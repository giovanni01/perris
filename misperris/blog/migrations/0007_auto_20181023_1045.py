# Generated by Django 2.1.2 on 2018-10-23 13:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20181017_2146'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='postulante',
            options={'permissions': (('postulante', 'Postulante'),)},
        ),
    ]
