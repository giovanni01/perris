# Generated by Django 2.1.2 on 2018-10-08 18:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='Comuna',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='post',
            name='Region',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='post',
            name='correo',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='post',
            name='nombre',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='post',
            name='rut',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='post',
            name='tipoVivienda',
            field=models.CharField(max_length=20),
        ),
    ]
